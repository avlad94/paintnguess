# README #

### [UUS REPOSITOORIUM](https://github.com/AntonMatskevich/paintnguess) ###

-vanad lingid-

It's Paint'n'Guess's readme file.

**Prototype link** is the following: https://paintnguess.herokuapp.com/

[Projektiplaan](https://bitbucket.org/avlad94/paintnguess/wiki/Projektiplaan).

Paint'n'Guess is the word guessing game with two game modes:

* The drawing mode allows you to play online with your teammate(s) against the other team. One of teammates gets list of words and draws to explain one of them so the second teammate has to guess it to score points.

* The card mode is offline version (would be downloadable) of Alias card game.

For guessing each team has one minute for a round.