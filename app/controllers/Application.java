package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {
    String dirtyLink = "https://www.swedbank.com/idc/groups/public/@i/@sbg/@gs/documents/logotype/cid_007184@t~a1.jpg";

    public Result index() {
        return ok(views.html.index.render());
    }

    public Result mode() {
        return ok(views.html.mode.render());
    }

    public Result aboutUs() {
        return ok(views.html.aboutus.render());
    }

    public Result card() {
        return ok(views.html.card.render());
    }

    public Result donate() {
        return ok(views.html.donate.render(dirtyLink));
    }

    public Result drawing() {
        return ok(views.html.drawing.render());
    }

    public Result drawingGame() {
        return ok(views.html.drawing_game.render());
    }

    public Result login() {
        return ok(views.html.login.render());
    }

    public Result signup() {
        return ok(views.html.signup.render());
    }



}
